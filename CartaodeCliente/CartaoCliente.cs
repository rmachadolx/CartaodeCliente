﻿namespace CartaodeCliente
{
    using System;
    using System.Text;

  
    public class CartaoCliente
    {
        int[] _conta = new int[50];

        #region Atributos

        private string _codigo;

        private string _nome;

        private double[] _compra=new double[50];

        private string[] _descricao= new string[50];

        private int _pontos;

        private double _totalCompras;

        private int _numeroDeCompra;


        #endregion

        #region Propriedadades



        public string Codigo
        {
            get { return _codigo; }
            set { _codigo = value; }
        }
        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
      
        public int Pontos
        {
            get { return _pontos; }
            set { _pontos = value; }
        }
        public double TotalCompras
        {
            get { return _totalCompras; }
            set { _totalCompras = value; }
        }


        public int NumeroDeCompra
        {
            get { return _numeroDeCompra; }
            set { _numeroDeCompra = value; }
        }


        #endregion
        #region Metodos
        //construtores
        public CartaoCliente(CartaoCliente cartao) : this(cartao.Codigo, cartao.Nome, cartao.Pontos, cartao.TotalCompras) { }
        public CartaoCliente(string _codigo, string _nome, int _pontos, double _totalCompras)
        {
            this.Codigo = _codigo;
            this.Nome = _nome;
           
            this.Pontos = _pontos;
            this.TotalCompras = _totalCompras;
        }

      

        public CartaoCliente Clone(CartaoCliente cartao)
        {
            Codigo = cartao.Codigo;
            Nome = cartao.Nome;
            
            Pontos = cartao.Pontos;
            TotalCompras = cartao.TotalCompras;
            return this;
        }
        //fim dos contrutores



        //- Alterar taxa de prémio,
        //alterar titular do cartão, 
        //    atribuir pontos a um cartão, ok
        //    desconta pontos do cartão,ok
        //    efetua compra de uma dado valor e atualiza os pontos
        //    descrição e o total de compras,ok
        //    passa n pontos de um cartão para o recetor.
        public void AtribuirPontos(double valor)
        {
            if (_pontos % 50 == 0)
            {
                _pontos += 5;
            }
            _pontos += Convert.ToInt32(Math.Floor(valor * 0.1));

        }
        public void EfetuarCompra(double valor, string descritivo)

        {
           
            _compra[_numeroDeCompra] = valor;
            _totalCompras += _compra[_numeroDeCompra];
            _descricao[_numeroDeCompra] = descritivo;

            ++_numeroDeCompra;
            if (_pontos % 50 == 0)
            {
                _pontos += 5;
            }
            _pontos += Convert.ToInt32(Math.Floor(valor * 0.1));
        }
        public void DescontaPontos(int valor)
        {
            _pontos -= valor;
        }
        public void ListaCompras()
        {
            string[,] matriz = new string[_numeroDeCompra, 2];
            for (int i = 0; i < _numeroDeCompra; i++)
            {
                matriz[i, 0] = _descricao[i];
                matriz[i, 1] = _compra[i].ToString();
            }
        }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();

            s.Append("--------Dados do Cartão----------");
            s.Append("Código do cartão: " + Codigo + "\n");
            s.Append("Nome: " + Nome + "\n");
            s.Append("Pontos: " + Pontos + "\n");
            s.Append("\n********Histórico do cliente********\n");
            for (int i = 0; i < _numeroDeCompra; i++)
            {
                s.Append("Valor da Compra; " +_compra[i] + "\n");
                s.Append("Descricao:" + _descricao[i]+"\n");
                s.Append("--------------------------------------\n");
            }
            
            s.Append("\n Total de Compras " + TotalCompras + "\n");
            s.Append("Número de compras: " + NumeroDeCompra + "\n");



            return s.ToString();
        }
        #endregion;
    }
}
