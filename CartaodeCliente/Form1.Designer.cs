﻿namespace CartaodeCliente
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label2 = new System.Windows.Forms.Label();
            this.TextBoxCodigo = new System.Windows.Forms.TextBox();
            this.TextBoxNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonCriarCartao = new System.Windows.Forms.Button();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxValorCompra = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextBoxDescricaoCompra = new System.Windows.Forms.TextBox();
            this.ButtonComprar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(27, 28);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(112, 16);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Código Cartão:";
            // 
            // TextBoxCodigo
            // 
            this.TextBoxCodigo.Location = new System.Drawing.Point(160, 28);
            this.TextBoxCodigo.Name = "TextBoxCodigo";
            this.TextBoxCodigo.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCodigo.TabIndex = 1;
            // 
            // TextBoxNome
            // 
            this.TextBoxNome.Location = new System.Drawing.Point(160, 87);
            this.TextBoxNome.Name = "TextBoxNome";
            this.TextBoxNome.Size = new System.Drawing.Size(100, 20);
            this.TextBoxNome.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nome do Cliente:";
            // 
            // ButtonCriarCartao
            // 
            this.ButtonCriarCartao.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonCriarCartao.Location = new System.Drawing.Point(160, 168);
            this.ButtonCriarCartao.Name = "ButtonCriarCartao";
            this.ButtonCriarCartao.Size = new System.Drawing.Size(100, 56);
            this.ButtonCriarCartao.TabIndex = 4;
            this.ButtonCriarCartao.Text = "Criar Cartão";
            this.ButtonCriarCartao.UseVisualStyleBackColor = true;
            this.ButtonCriarCartao.Click += new System.EventHandler(this.ButtonCriarCartao_Click);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelStatus.Location = new System.Drawing.Point(41, 245);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(0, 16);
            this.LabelStatus.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(351, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Valor da compra:";
            // 
            // TextBoxValorCompra
            // 
            this.TextBoxValorCompra.Location = new System.Drawing.Point(568, 24);
            this.TextBoxValorCompra.Name = "TextBoxValorCompra";
            this.TextBoxValorCompra.Size = new System.Drawing.Size(100, 20);
            this.TextBoxValorCompra.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(299, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "Desccrição da compra:";
            // 
            // TextBoxDescricaoCompra
            // 
            this.TextBoxDescricaoCompra.Location = new System.Drawing.Point(484, 83);
            this.TextBoxDescricaoCompra.Name = "TextBoxDescricaoCompra";
            this.TextBoxDescricaoCompra.Size = new System.Drawing.Size(184, 20);
            this.TextBoxDescricaoCompra.TabIndex = 9;
            // 
            // ButtonComprar
            // 
            this.ButtonComprar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonComprar.Location = new System.Drawing.Point(568, 168);
            this.ButtonComprar.Name = "ButtonComprar";
            this.ButtonComprar.Size = new System.Drawing.Size(75, 56);
            this.ButtonComprar.TabIndex = 10;
            this.ButtonComprar.Text = "Comprar";
            this.ButtonComprar.UseVisualStyleBackColor = true;
            this.ButtonComprar.Click += new System.EventHandler(this.ButtonComprar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 594);
            this.Controls.Add(this.ButtonComprar);
            this.Controls.Add(this.TextBoxDescricaoCompra);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBoxValorCompra);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.ButtonCriarCartao);
            this.Controls.Add(this.TextBoxNome);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxCodigo);
            this.Controls.Add(this.Label2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.TextBox TextBoxCodigo;
        private System.Windows.Forms.TextBox TextBoxNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonCriarCartao;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxValorCompra;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TextBoxDescricaoCompra;
        private System.Windows.Forms.Button ButtonComprar;
    }
}

