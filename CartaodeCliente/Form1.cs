﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CartaodeCliente
{
    public partial class Form1 : Form
    {
        CartaoCliente cartao = new CartaoCliente("","",0,0 );
        int[] _conta = new int[50];
        public Form1()
        {
            InitializeComponent();
            TextBoxDescricaoCompra.Enabled = false;
            TextBoxValorCompra.Enabled = false;
            ButtonComprar.Enabled = false;
        }



        private void ButtonCriarCartao_Click(object sender, EventArgs e)
        {
            if (TextBoxCodigo.Text == "") // Valida se Preço do litro é double
            {
                MessageBox.Show("Introduza o código de cartão.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxCodigo.Focus();
            }
            else if (TextBoxNome.Text == "")
            {
                MessageBox.Show("Introduza o nome do cliente.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxNome.Focus();
            }
            else
            {
                cartao = new CartaoCliente(TextBoxCodigo.Text,TextBoxNome.Text, 0, 0);
                LabelStatus.Text = cartao.ToString();
                TextBoxDescricaoCompra.Enabled = true;
                TextBoxValorCompra.Enabled =true;
                ButtonComprar.Enabled = true;
                TextBoxNome.Enabled = false;
                TextBoxCodigo.Enabled = false;
                ButtonCriarCartao.Enabled = false;
                LabelStatus.Text= cartao.ToString();

            }
        }

        private void ButtonComprar_Click(object sender, EventArgs e)
        {
            double valorcompra;
            if (!double.TryParse(TextBoxValorCompra.Text, out valorcompra))
            {
                MessageBox.Show("Introduza um valor correcto!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxValorCompra.Focus();
            }
            else if (TextBoxDescricaoCompra.Text == "")
            {
                MessageBox.Show("Introduza o código nome do cliente.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                TextBoxDescricaoCompra.Focus();
            }
            else
            {
             
                cartao.EfetuarCompra(Convert.ToDouble(TextBoxValorCompra.Text), TextBoxDescricaoCompra.Text);
                LabelStatus.Text = cartao.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cartao.ListaCompras();
        }
    }
}
